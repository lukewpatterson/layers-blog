package org.lukewpatterson.layers.blog.app;

import org.lukewpatterson.layers.blog.snapshot.dependency.ClassFromSnapshotDependency;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class OurSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(OurSpringBootApplication.class, args);
    }
    
    @RequestMapping("/")
    public String index() {
        return String.format("Value from App is [%s]. Value from Snapshot Dependency is [%s]", //
                "app-rev-2", //
                ClassFromSnapshotDependency.SOME_VALUE //
        );
    }

}
