FROM maven:3.5.4-jdk-8-slim as build
# ignore until next comment, use tricks and aspects of layers unrelated to this blog to help reduce repeated build times
RUN apt-get update -qq && apt-get install -qq --assume-yes tree
WORKDIR /layers-blog-modules/
COPY ./pom.xml ./pom.xml
COPY ./layers-blog-snapshot-dependency/pom.xml ./layers-blog-snapshot-dependency/pom.xml
COPY ./layers-blog-app/pom.xml ./layers-blog-app/pom.xml
# ignore errors for the following command
RUN mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.1:go-offline -Dorg.slf4j.simpleLogger.defaultLogLevel=OFF --strict-checksums --batch-mode --fail-never
RUN mvn de.qaware.maven:go-offline-maven-plugin:1.0.0:resolve-dependencies --quiet --strict-checksums --batch-mode

#
# maven-build the snapshot and app modules 
#
WORKDIR ./layers-blog-snapshot-dependency/
COPY ./layers-blog-snapshot-dependency/ ./
RUN mvn install --quiet --update-snapshots --strict-checksums --batch-mode
WORKDIR ../layers-blog-app/
COPY layers-blog-app ./
RUN mvn package --quiet --update-snapshots --strict-checksums --batch-mode
WORKDIR ./target/
#
# https://docs.spring.io/spring-boot/docs/2.0.5.RELEASE/reference/htmlsingle/#executable-jar-jar-file-structure
# https://docs.spring.io/spring-boot/docs/2.0.5.RELEASE/reference/htmlsingle/#executable-jar-exploded-archives
# unzip and pre-separate into layers
#
RUN unzip -q *.jar -d ./unzipped/ \
    # display exploded jar structure
    && tree ./unzipped/ \
    # layer 3 - app-layer classes, resources and meta
    && mkdir --parents /layers/3-app/BOOT-INF/ \
    && mv ./unzipped/BOOT-INF/classes/ /layers/3-app/BOOT-INF/ \
    && mv ./unzipped/META-INF/ /layers/3-app/ \
    # layer 2 - snapshot dependencies
    && mkdir --parents /layers/2-dependencies-snapshot/BOOT-INF/lib/ \
    && find ./unzipped/BOOT-INF/lib/ -type f -name '*-SNAPSHOT.jar' -exec mv {} /layers/2-dependencies-snapshot/BOOT-INF/lib/ ';' \
    # layer 1 - (everything else) release dependencies and jarlauncher, also any items not accounted for in other layers
    && mv ./unzipped/ /layers/1-dependencies-release/ \
    # display exploded jar structure, separated into layers
    && tree /layers/

FROM openjdk:8u181-alpine3.8 as run
# ignore until next comment
RUN apk add tree
# reconstitute exploded jar structure
WORKDIR /app/
COPY --from=build /layers/1-dependencies-release/ ./
COPY --from=build /layers/2-dependencies-snapshot/ ./
COPY --from=build /layers/3-app/ ./
# display reconstituted exploded jar structure 
RUN tree
# https://docs.spring.io/spring-boot/docs/2.0.5.RELEASE/reference/htmlsingle/#executable-jar-exploded-archives
CMD ["java", "org.springframework.boot.loader.JarLauncher"]
