# Gaining Docker Image Size Efficiencies By Separating App Layers

## Problem
I was **pushing a new Docker image tag for each app code commit**, and the admins of the private registry were getting annoyed at **how much space I was using**.

## Solution Summary
Yes, I know there are strategies to clean up old tags but I first wanted to reduce the impact of the tags I was pushing.
With the right layering strategy, I knew I could reduce the net registry size increase of consecutive tag pushes.
I wanted to **only push what had actually changed in the app**.
In addition to reducing the impact on the registry, having smaller tag deltas could possibly speed up rolling deployments since nodes could potentially have less to download.

## Qualifiers
I don't know how helpful or applicable my strategies will be your specific app, so let's get the specifics of my app out of the way:
* Docker (obviously)
* Spring Boot
* Maven
* Java
* Most of the day-to-day changes in the app occur in classes directly found in the top-most project, the project containing the @SpringBootApplication class.
If this isn't the case in your app, then a different "separation strategy" might apply.

## Solution Details
**FYI - There is executable example code.**
So if the level of detail in the following steps is lacking, checking out the code and playing around with it is one good way to fill in the gaps. 
[This code repo's commit history](https://gitlab.com/lukewpatterson/layers-blog/commits/master) contains some commits which illustrate the correlation of changes in different parts of the codebase with how that affects the layer caching behavior.
The details that follow revolve around those commits.
You'll see that commit diffs also include the relevant command line execution logs.
_A link labelled "line link" points to a specific line in the code, so when following the link look for the highlighted line._

### Step 0 - example codebase and layering pattern
**[[commit link]](https://gitlab.com/lukewpatterson/layers-blog/commit/b663ee2033f79df501fbf7f1bc3eb112861a786b)**  _Pro-Tip: you can press `t` to see a different view of the overall file structure_

* A Maven multi-module project so it's easy to show the effects of changing different layers of the codebase.  _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/b663ee2033f79df501fbf7f1bc3eb112861a786b#442292b8a7efeabbe4cc176709b833b1792140ec_0_8)_
* reproducible-build-maven-plugin to ensure JARs only change when there is an actual code change - _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/b663ee2033f79df501fbf7f1bc3eb112861a786b#33e9409d56d7045f00e4b58583e284a1a02c09bf_0_37)_
  * Documentation - [https://zlika.github.io/reproducible-build-maven-plugin/]()
* Spring Boot's "exploded archive"-style of executable since a "non-exploded" archive (a JAR file) is opaque to Docker's layer caching mechanism. _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/b663ee2033f79df501fbf7f1bc3eb112861a786b#6651ddff6eb82c840ced7c1dddee15c6e1913dd4_0_27)_
  * Executable Jar Documentation - [https://docs.spring.io/spring-boot/docs/2.0.5.RELEASE/reference/htmlsingle/#executable-jar-jar-file-structure]()
  * Exploded Archive Documentation - [https://docs.spring.io/spring-boot/docs/2.0.5.RELEASE/reference/htmlsingle/#executable-jar-exploded-archives]()
* From the execution log, the layout of the exploded archive:
  ```text
     /unzipped/
    ├── BOOT-INF
    │   ├── classes
    │   │   ├── application.properties
    │   │   └── org
    │   │       └── lukewpatterson
    │   │           └── layers
    │   │               └── blog
    │   │                   └── app
    │   │                       └── OurSpringBootApplication.class
    │   └── lib
    │       ...
    │       ├── jul-to-slf4j-1.7.25.jar
    │       ├── layers-blog-snapshot-dependency-1-SNAPSHOT.jar
    │       ├── log4j-api-2.10.0.jar
    │       ...
    │       ├── spring-web-5.0.9.RELEASE.jar
    │       ├── spring-webmvc-5.0.9.RELEASE.jar
    │       └── validation-api-2.0.1.Final.jar
    ├── META-INF
    │   ├── MANIFEST.MF
    │   └── maven
    │       └── org.lukewpatterson
    │           └── layers-blog-app
    │               ├── pom.properties
    │               └── pom.xml
    └── org
        └── springframework
            └── boot
                └── loader
                    ├── ExecutableArchiveLauncher.class
                    ├── JarLauncher.class
                    ├── ...
                    └── util
                        └── SystemPropertyUtils.class

   ```
* Separation of exploded archive layers in the build image, separation progresses from "changes most often" to "changes least often". _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/b663ee2033f79df501fbf7f1bc3eb112861a786b#6651ddff6eb82c840ced7c1dddee15c6e1913dd4_0_30)_
  * The other steps will refer to these layers.
  * From the execution log, the layout of layers once separated:
  ```text
    /layers/
    ├── 1-dependencies-release
    │   ├── BOOT-INF
    │   │   └── lib
    │   │       ...
    │   │       ├── spring-beans-5.0.9.RELEASE.jar
    │   │       ├── spring-boot-2.0.5.RELEASE.jar
    │   │       ├── spring-boot-autoconfigure-2.0.5.RELEASE.jar
    │   │       ...
    │   │       └── validation-api-2.0.1.Final.jar
    │   └── org
    │       └── springframework
    │           └── boot
    │               └── loader
    │                   ├── ExecutableArchiveLauncher.class
    │                   ├── JarLauncher.class
    │                   ...
    │                   └── util
    │                       └── SystemPropertyUtils.class
    ├── 2-dependencies-snapshot
    │   └── BOOT-INF
    │       └── lib
    │           └── layers-blog-snapshot-dependency-1-SNAPSHOT.jar
    └── 3-app
        ├── BOOT-INF
        │   └── classes
        │       ├── application.properties
        │       └── org
        │           └── lukewpatterson
        │               └── layers
        │                   └── blog
        │                       └── app
        │                           └── OurSpringBootApplication.class
        └── META-INF
            ├── MANIFEST.MF
            └── maven
                └── org.lukewpatterson
                    └── layers-blog-app
                        ├── pom.properties
                        └── pom.xml
   ```
* Reconstitution of layers in the runtime image.
Reconstitution progresses from "changes least often" to "changes most often". _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/b663ee2033f79df501fbf7f1bc3eb112861a786b#6651ddff6eb82c840ced7c1dddee15c6e1913dd4_0_46)_
  * From the execution log, the layout once reconstituted: (notice it's the same as the 'unzipped' form)
  ```text
    .
    ├── BOOT-INF
    │   ├── classes
    │   │   ├── application.properties
    │   │   └── org
    │   │       └── lukewpatterson
    │   │           └── layers
    │   │               └── blog
    │   │                   └── app
    │   │                       └── OurSpringBootApplication.class
    │   └── lib
    │       ...
    │       ├── jul-to-slf4j-1.7.25.jar
    │       ├── layers-blog-snapshot-dependency-1-SNAPSHOT.jar
    │       ├── log4j-api-2.10.0.jar
    │       ...
    │       ├── spring-web-5.0.9.RELEASE.jar
    │       ├── spring-webmvc-5.0.9.RELEASE.jar
    │       └── validation-api-2.0.1.Final.jar
    ├── META-INF
    │   ├── MANIFEST.MF
    │   └── maven
    │       └── org.lukewpatterson
    │           └── layers-blog-app
    │               ├── pom.properties
    │               └── pom.xml
    └── org
        └── springframework
            └── boot
                └── loader
                    ├── ExecutableArchiveLauncher.class
                    ├── JarLauncher.class
                    ├── ...
                    └── util
                        └── SystemPropertyUtils.class

   ```
* From the execution log, you can see that no app codebase layers were already cached since this is the first build.
The absence of a ` ---> Using cache` following a `COPY` command here means that the layer has never been built before.
**This is the key to the whole thing.  Watch what happens in this area on the steps that follow.**
  ```text
  Step 20/24 : COPY --from=build /layers/1-dependencies-release/ ./
   ---> d158e97b92fe
  Step 21/24 : COPY --from=build /layers/2-dependencies-snapshot/ ./
   ---> bb5e6342aa80
  Step 22/24 : COPY --from=build /layers/3-app/ ./
   ---> e0395252c3b1
  ```

### Step 1 - change a release dependency
**[[commit link]](https://gitlab.com/lukewpatterson/layers-blog/commit/9b0758c25473ac64cf3206f5efc1f7f513abd0db)**

* Added a release dependency. _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/9b0758c25473ac64cf3206f5efc1f7f513abd0db#33e9409d56d7045f00e4b58583e284a1a02c09bf_27_27)_
* This is the least likely layer to change, and this strategy isn't optimized for this, so this type of change doesn't illustrate any size savings for registry pushes or consumers pulls.
* From the execution log, you can see that the ` ---> Using cache` situation isn't any better.

### Step 2 - change a snapshot dependency
**[[commit link]](https://gitlab.com/lukewpatterson/layers-blog/commit/c3e137b7e67ed8c2462912803a709fd84bfff26b)**

* Changed a snapshot dependency. _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/c3e137b7e67ed8c2462912803a709fd84bfff26b#9d6f8345181bcf61542da52e366840f0584f985a_5_5)_
* During development it's quite common for snapshot dependencies to get updated.
* From the execution log, you can see that the ` ---> Using cache` situation is getting better:
  ```text
    Step 20/24 : COPY --from=build /layers/1-dependencies-release/ ./
     ---> Using cache
     ---> a5f34ee04615
    Step 21/24 : COPY --from=build /layers/2-dependencies-snapshot/ ./
     ---> 7446b0325602
    Step 22/24 : COPY --from=build /layers/3-app/ ./
     ---> 6b0cd535260f
  ```
* A push to the private registry at this point, assuming previous tags had been pushed, would **not require the duplication of the layer containing the release dependencies**.
In most apps, the release dependency layer is by far the largest of the 3 types identified in these steps.

### Step 3 - change the app's top-level code
**[[commit link]](https://gitlab.com/lukewpatterson/layers-blog/commit/cc477c200ba09c7ff2258f5f26b15f31bc72f78f)**

* Change some code in the top-level app. _[[line link]](https://gitlab.com/lukewpatterson/layers-blog/commit/cc477c200ba09c7ff2258f5f26b15f31bc72f78f#c67dc30dbe45d58472b42cbbd4b9729cbe50e87a_20_20)_
* Of the 3 layers identified in these steps, the layer most frequently changed  
* From the execution log, you can see that the ` ---> Using cache` situation is getting better:
  ```text
    Step 20/24 : COPY --from=build /layers/1-dependencies-release/ ./
     ---> Using cache
     ---> a5f34ee04615
    Step 21/24 : COPY --from=build /layers/2-dependencies-snapshot/ ./
     ---> Using cache
     ---> 7446b0325602
    Step 22/24 : COPY --from=build /layers/3-app/ ./
     ---> 70d1d9f84c14
  ```
* A push to the private registry at this point, assuming previous tags had been pushed, would **not require the duplication of the layers containing the release dependencies or the snapshot dependencies**.

## Closing Thoughts
There can be size and deployment advantages of separating your app into layers of differing change frequencies.  The pattern can be a little fragile if the app's internal layout changes, but a "failure" is simply a loss of the efficiencies gained by layering and not a failure to include pieces of the app.  I know there are many areas in this walkthrough that would be improved by more detail.  I'd like to have written a book, but didn't have time.  I just wrote what I could and hopefully you find something helpful in the code and patterns outlined here.